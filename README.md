# FetchRewards - REST API

Fetch Rewards Coding Exercise

Project is developed using Java Spring Boot

## Please download the following pre-requisites

    * Java 8
    * Maven
    * IntelliJ/Eclipse/STS
    * Postman/Any API testing tool/Command Line

## Running the Sprint Boot App

### 1. Open the terminal to the project root and run the following command:
```
mvn spring-boot:run
```

### 2. Open IntelliJ
```
Open IntelliJ, click "File -> Open", select the "FetchRewards" project folder, and run FetchRewardsApiApplication.
```

## Using the App APIs

### 1. Open Postman and import the json collection file provided in the repository

**[GET]** http://localhost:8081/payer/getHome
```
Response Status: Success/Invalid 

Response Body:
Application running. Please look at ReadMe to understand the working	
```

**[POST]** http://localhost:8081/payer/addPoints
```
Request Body:
{ 
    "payer": "DANNON", 
    "points": 1000, 
    "timestamp": "2020-11-02T14:00:00Z" 
}
Request Headers: 
[{"key":"Content-Type","value":"application/json"}]

Response Status: Success/Invalid 

Response Body:
{ 
    "payer": "DANNON", 
    "points": 1000, 
    "timestamp": "2020-11-02T14:00:00Z" 
}

```

**[POST]** http://localhost:8081/payer/spendPoints
```
Request Body:
{
     "points": 5000 
}
Request Headers: 
[{"key":"Content-Type","value":"application/json"}]

Response Status: Success/Invalid 

Response Body: [
    		{ "payer": "DANNON", "points": -100 },
    		{ "payer": "UNILEVER", "points": -200 },
    		{ "payer": "MILLER COORS", "points": -4,700 }
		]
```

**[GET]** http://localhost:8081/payer/getPoints
```
Response Status: Success/Invalid 

Response Body:
[
    { "payer": "DANNON", "points":1000 },
	{ "payer": "UNILEVER", "points":0 },
	{ "payer": "MILLER COORS", "points":5300 }
	
]		
```



### 2. Examples with curl

### Application Running status
```
curl -i \
    -H "Content-Type:application/json" \
    http://localhost:8081/payer/getHome
``` 


#### Add Points Transaction
```
curl -i \
    -H "Content-Type:application/json" \
    -X POST --data '{ "payer": "DANNON", "points": 1000, "timestamp": "2020-11-02T14:00:00Z" }' \
    http://localhost:8081/payer/addPoints
```

#### Spend Points
```
curl -i \
    -H "Content-Type:application/json" \
    -X POST --data '{ "points": 5000 }' \
    http://localhost:8081/payer/spendPoints
```

#### Get Payer Balance
```
curl -i \
    -H "Content-Type:application/json" \
    http://localhost:8081/payer/getPoints
``` 


## Running the Tests

### 1. Java 8 Command Line
Open a terminal to the project root, make sure Java 11 is on the execution path and run:
Open the terminal to the project root and run the following command:
```
mvn test -DrunSuite=**/UnitTestSuite.class -DfailIfNoTests=true
```

### 2. IntelliJ
Once project has been opened in IntelliJ IDE, go to src/test/UnitTestSuite file and Run it


