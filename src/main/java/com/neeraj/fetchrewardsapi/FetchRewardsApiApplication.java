package com.neeraj.fetchrewardsapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.neeraj.fetchrewardsapi"})
public class FetchRewardsApiApplication {

    private static final Logger logger = LoggerFactory.getLogger(FetchRewardsApiApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(FetchRewardsApiApplication.class, args);
    }

}
