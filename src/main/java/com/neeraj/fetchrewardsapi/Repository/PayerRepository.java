package com.neeraj.fetchrewardsapi.Repository;

import com.neeraj.fetchrewardsapi.Model.Payer;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PayerRepository extends JpaRepository<Payer, Long> {

    @Query("SELECT user FROM Payer user WHERE user.payer = :payer")
    List<Payer> findPayer(@Param("payer") String payer);

    @Transactional
    @Modifying
    @Query("UPDATE Payer user SET user.points = user.points + :points WHERE user.payer = :payer")
    void updatePayersBalance(@Param("payer") String payer, @Param("points") int points);
}

