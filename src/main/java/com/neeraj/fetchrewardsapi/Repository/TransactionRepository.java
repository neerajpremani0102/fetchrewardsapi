package com.neeraj.fetchrewardsapi.Repository;

import java.util.List;
import javax.transaction.Transactional;

import com.neeraj.fetchrewardsapi.Model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("SELECT user FROM Transaction user WHERE user.payer = :payer")
    List<Transaction> findByPayer(@Param("payer") String payer);

    @Query("SELECT points FROM Transaction points ORDER BY points.timestamp ASC")
    List<Transaction> getPointsAscOrder();

    @Transactional
    @Modifying
    @Query("UPDATE Transaction user SET user.points = :pointsToUpdate WHERE user.id = :id")
    void updateTransactionsPoints(@Param("pointsToUpdate") int points,
                                  @Param("id") Long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Transaction transaction WHERE transaction.id = :id")
    void removeTransaction(@Param("id") Long id);
}
