package com.neeraj.fetchrewardsapi.Service;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.neeraj.fetchrewardsapi.Model.Payer;
import com.neeraj.fetchrewardsapi.Model.Points;
import com.neeraj.fetchrewardsapi.Model.Transaction;
import com.neeraj.fetchrewardsapi.Repository.PayerRepository;
import com.neeraj.fetchrewardsapi.Repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class FetchRewardsSvcImpln implements FetchRewardsSvc {

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private PayerRepository payerRepository;

    /**
     * @param points
     * @return the amount of remaining points for each payer
     */
    @Override
    public List<Payer> spendPoints(Points points) {

        List<Transaction> sortedPointsByDate = transactionRepository.getPointsAscOrder();
        int subtractPoints = points.getPoints();
        PointsSubtractHelper pointsSubtractHelper = new PointsSubtractHelper();

        for (Transaction transaction : sortedPointsByDate) {

            if (subtractPoints == 0) {
                break;
            } else if (transaction.getPoints() < subtractPoints) {
                subtractPoints -= transaction.getPoints();
                pointsSubtractHelper.subtractPoints(transaction.getPayer(), transaction.getPoints());
                transactionRepository.removeTransaction(transaction.getId());
            } else {
                int pointsToUpdate = transaction.getPoints() - subtractPoints;
                pointsSubtractHelper.subtractPoints(transaction.getPayer(), subtractPoints);
                transactionRepository.updateTransactionsPoints(pointsToUpdate, transaction.getId());
                subtractPoints = 0;
            }
        }

        subtractPointsPayer(pointsSubtractHelper);
        List<Payer> payerListPointsSpent = pointsSubtractHelper.getPointsSubtractedPayer();
        return payerListPointsSpent;
    }

    /**
     * @param pointsSubtractHelper
     * contains the points spent for each payer
     */
    private void subtractPointsPayer(PointsSubtractHelper pointsSubtractHelper) {
        for (Map.Entry<String, Integer> entry : pointsSubtractHelper.getPointsSubtracted().entrySet()) {
            payerRepository.updatePayersBalance(entry.getKey(), entry.getValue());
        }
    }

    /**
     * This method updates the Payer points if present or else adds a saves a new Payer with the points
     * It also saves the transaction in the TransactionRepo
     *
     * @param transaction
     * @return transaction after saving it
     */
    @Override
    public Transaction addPoints(Transaction transaction) {

        if (!payerRepository.findPayer(transaction.getPayer()).isEmpty()) {
            payerRepository.updatePayersBalance(transaction.getPayer(), transaction.getPoints());
        } else {
            payerRepository.save(new Payer(transaction.getPayer(), transaction.getPoints()));
        }
        Transaction savedTransaction = transactionRepository.save(transaction);
        return savedTransaction;
    }

    @Override
    public List<Transaction> getAllTransactions() {
        return transactionRepository.findAll();
    }

    @Override
    public List<Payer> getPayerBalance() throws JsonProcessingException {
        return payerRepository.findAll();


    }


}
