package com.neeraj.fetchrewardsapi.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neeraj.fetchrewardsapi.Model.Payer;
import com.neeraj.fetchrewardsapi.Model.Points;
import lombok.Getter;

public class PointsSubtractHelper {


    @Getter
    Map<String, Integer> pointsSubtracted = new HashMap<>();

    public void subtractPoints(String payer, int pointsToSubtract) {

        pointsSubtracted.computeIfPresent(payer, (k, v) -> v + (-1 * pointsToSubtract));
        pointsSubtracted.putIfAbsent(payer, -1 * pointsToSubtract);
    }

    public List<Payer> getPointsSubtractedPayer() {

        List<Payer> pointsSpentPayer = new ArrayList<>();

        for (Map.Entry<String, Integer> entry : pointsSubtracted.entrySet()) {
            pointsSpentPayer.add(new Payer(entry.getKey(), entry.getValue()));
        }

        return pointsSpentPayer;
    }
}

