package com.neeraj.fetchrewardsapi.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.neeraj.fetchrewardsapi.Model.Payer;
import com.neeraj.fetchrewardsapi.Model.Transaction;
import com.neeraj.fetchrewardsapi.Model.Points;


import java.util.List;

public interface FetchRewardsSvc {

    List<Payer> spendPoints(Points points);

    Transaction addPoints(Transaction newTransaction);

    List<Transaction> getAllTransactions();

    List<Payer> getPayerBalance() throws JsonProcessingException;

}
