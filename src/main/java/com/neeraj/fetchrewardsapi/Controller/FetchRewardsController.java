package com.neeraj.fetchrewardsapi.Controller;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.neeraj.fetchrewardsapi.Model.Payer;
import com.neeraj.fetchrewardsapi.Model.Points;
import com.neeraj.fetchrewardsapi.Model.Transaction;
import com.neeraj.fetchrewardsapi.Service.FetchRewardsSvc;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/payer")
public class FetchRewardsController {

    @Autowired
    private FetchRewardsSvc fetchRewardsSvc;


    @GetMapping(value = "/getHome", produces = "application/json")
    String getHomeMessage() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode childNode = mapper.createObjectNode();
        childNode.put("Application Running", "Please look at ReadMe to understand the working");
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(childNode);
        return json;
    }

    @SneakyThrows
    @GetMapping(value = "/getPoints", produces = "application/json")
    String getPayerBalance() {
        List<Payer> allPayersList = fetchRewardsSvc.getPayerBalance();
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode childNode = mapper.createObjectNode();
        for(Payer p: allPayersList){
            childNode.put(p.getPayer(), p.getPoints());
        }
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(childNode);
        return json;
    }

    @PostMapping(path = "/addPoints", consumes = "application/json", produces = "application/json")
    Transaction addPoints(@RequestBody Transaction transaction) {
        return fetchRewardsSvc.addPoints(transaction);
    }

    @PostMapping(path = "/spendPoints", consumes = "application/json", produces = "application/json")
    List<Payer> spendPoints(@RequestBody Points points) {
        return fetchRewardsSvc.spendPoints(points);
    }
}
