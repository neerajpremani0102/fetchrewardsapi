package com.neeraj.fetchrewardsapi;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        FetchRewardsSvcImplnTests.class,
        FetchRewardsApiApplicationTests.class
})


public class UnitTestSuite {
}
