package com.neeraj.fetchrewardsapi;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.neeraj.fetchrewardsapi.Model.Payer;
import com.neeraj.fetchrewardsapi.Model.Points;
import com.neeraj.fetchrewardsapi.Model.Transaction;
import com.neeraj.fetchrewardsapi.Repository.PayerRepository;
import com.neeraj.fetchrewardsapi.Repository.TransactionRepository;
import com.neeraj.fetchrewardsapi.Service.FetchRewardsSvc;
import org.junit.jupiter.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class FetchRewardsSvcImplnTests {


    @Autowired
    private FetchRewardsSvc fetchRewardsSvc;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private PayerRepository payerRepository;

    @Test
    public void addPointsTest() {
        Transaction dannonTransaction = new Transaction(null, "DANNON", 1100, new Date());
        Transaction pointsAdded = fetchRewardsSvc
                .addPoints(dannonTransaction);

        Assertions.assertEquals(dannonTransaction, pointsAdded);
    }

    @Test
    public void getPayerBalanceTest() throws JsonProcessingException {

        fetchRewardsSvc.addPoints(new Transaction(null, "DANNON", 1200, new Date()));
        List<Payer> payerBalance = fetchRewardsSvc.getPayerBalance();

        Assertions.assertEquals(payerBalance.get(0), new Payer(1L, "DANNON", 1200));
    }

    @Test
    public void getTransactionTest() {

        Transaction transaction = new Transaction(null, "DANNON", 1300, new Date());
        fetchRewardsSvc.addPoints(transaction);
        List<Transaction> allTransactions = fetchRewardsSvc.getAllTransactions();
        Assertions.assertEquals(allTransactions.get(0).getPayer(), "DANNON");
        Assertions.assertEquals(allTransactions.get(0).getPoints(), 1300);
    }

    @Test
    public void spendPoints() throws JsonProcessingException {

        Transaction inTransaction = new Transaction(null, "DANNON", 200, new Date());
        fetchRewardsSvc.addPoints(inTransaction);

        Payer payerPointsSpentSubtract = fetchRewardsSvc.spendPoints(new Points(100)).get(0);
        Assertions.assertEquals(payerPointsSpentSubtract.getPoints(), -100);

        Payer payerBalanceLeft = fetchRewardsSvc.getPayerBalance().get(0);
        Assertions.assertEquals(payerBalanceLeft.getPoints(), 100);
    }
}
