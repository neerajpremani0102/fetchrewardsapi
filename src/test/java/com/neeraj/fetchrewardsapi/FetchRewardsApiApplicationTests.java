package com.neeraj.fetchrewardsapi;

import com.neeraj.fetchrewardsapi.Controller.FetchRewardsController;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class FetchRewardsApiApplicationTests {


    @Autowired
    private FetchRewardsController fetchRewardsController;

    @LocalServerPort
    private int port;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void testEntireFlow() throws Exception {

        String reqURL = "http://localhost:" + port + "/payer";

        HttpHeaders headers = new HttpHeaders();
        headers.set("content-type", "application/json");
        //list containing the transactions
        List<String> list = Arrays.asList(
                "{ \"payer\": \"DANNON\", \"points\": 1000, \"timestamp\": \"2020-11-02T14:00:00Z\" }",
                "{ \"payer\": \"UNILEVER\", \"points\": 200, \"timestamp\": \"2020-10-31T11:00:00Z\" }",
                "{ \"payer\": \"DANNON\", \"points\": -200, \"timestamp\": \"2020-10-31T15:00:00Z\" }",
                "{ \"payer\": \"MILLER COORS\", \"points\": 10000, \"timestamp\": \"2020-11-01T14:00:00Z\" }",
                "{ \"payer\": \"DANNON\", \"points\": 300, \"timestamp\": \"2020-10-31T10:00:00Z\" }");

        for (String transaction : list) {
            HttpEntity<String> request = new HttpEntity<>(
                    transaction, headers);

            this.testRestTemplate.postForEntity(reqURL  + "/addPoints", request,
                    String.class);
        }



        // Spend Points Post request

        HttpEntity<String> request = new HttpEntity<>(
                "{ \"points\": 5000 }", headers);

        ResponseEntity<String> spendPointsResponse = this.testRestTemplate
                .postForEntity(reqURL  + "/spendPoints", request,
                        String.class);
        Assertions.assertEquals(spendPointsResponse.getStatusCode(), HttpStatus.OK);
        Assertions.assertTrue(
                spendPointsResponse.getBody().contains("{\"payer\":\"UNILEVER\",\"points\":-200}"));
        Assertions.assertTrue(
                spendPointsResponse.getBody().contains("{\"payer\":\"MILLER COORS\",\"points\":-4700}"));
        Assertions.assertTrue(
                spendPointsResponse.getBody().contains("{\"payer\":\"DANNON\",\"points\":-100}"));



        // Get Points after SpendPoints

        ResponseEntity<String> getPointsResponse = this.testRestTemplate
                .getForEntity(reqURL  + "/getPoints", String.class);

        Assertions.assertEquals(getPointsResponse.getStatusCode(), HttpStatus.OK);
        Assertions
                .assertTrue(getPointsResponse.getBody().contains("\"DANNON\" : 1000,"));
        Assertions
                .assertTrue(getPointsResponse.getBody().contains("\"UNILEVER\" : 0,"));
        Assertions.assertTrue(
                getPointsResponse.getBody().contains("\"MILLER COORS\" : 5300"));
    }

}
